#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2018 mpre <mpre@e-fed-ssd.enet.priv>
#
# Distributed under terms of the MIT license.

"""
Welcome to Check Rabbit access.

This tool is used to check credentials.
"""
import argparse
import socket
from kombu import Connection

# define and parse command-line options
parser = argparse.ArgumentParser(
    description='Check connection to RabbitMQ server')
parser.add_argument('--server', required=True, help='Define RabbitMQ server')
parser.add_argument('--vhost', default='/', help='Define virtual host')
parser.add_argument('--ssl', action='store_true',
                    help='Enable SSL (default: %(default)s)')
parser.add_argument('--port', type=int, default=5672,
                    help='Define port (default: %(default)s)')
parser.add_argument('--username', default='guest',
                    help='Define username (default: %(default)s)')
parser.add_argument('--password', default='guest',
                    help='Define password (default: %(default)s)')
args = vars(parser.parse_args())
args['proto'] = 'amqps' if args['ssl'] else 'amqp'

url = '{0}://{1}:{2}@{3}:{4}/{5}'.format(args['proto'], args['username'],
                                         args['password'], args['server'],
                                         args['port'], args['vhost'])
print "Connecting to '%s':" % url
print "Username: %s" % args['username']
print "Password: %s" % args['password']
print ""
with Connection(url) as c:
    try:
        c.connect()
    except socket.error:
        raise ValueError("Received socket.error, "
                         "rabbitmq server probably isn't running")
    except IOError:
        raise ValueError("Received IOError, probably bad credentials")
    else:
        print "Credentials are valid"
