#! /bin/env/python
# Original work: https://gist.github.com/christianclinton/faa1aef119a0919aeb2e

"""Rabbit MQ password hash helper."""

import hashlib
import binascii
# needed for password generation
import random
import os
import base64
# import struct
import string

# Utility methods for generating and comparing RabbitMQ user password hashes.
#
# Rabbit Password Hash Algorithm:
#
# Generate a random 32 bit salt:
# CA D5 08 9B

# Concatenate that with the UTF-8 representation of the password (in this
# case "simon"):
# CA D5 08 9B 73 69 6D 6F 6E

# Take the MD5 hash:
# CB 37 02 72 AC 5D 08 E9 B6 99 4A 17 2B 5F 57 12

# Concatenate the salt again:
# CA D5 08 9B CB 37 02 72 AC 5D 08 E9 B6 99 4A 17 2B 5F 57 12

# And convert to base64 encoding:
# ytUIm8s3AnKsXQjptplKFytfVxI=
#
# Sources:
# http://rabbitmq.1065348.n5.nabble.com/Password-Hashing-td276.html
# http://hg.rabbitmq.com/rabbitmq-server/file/df7aa5d114ae/src/rabbit_auth_backend_internal.erl#l204

# Test Case:
#   print encode_rabbit_password_hash('CAD5089B', "simon")
#   print decode_rabbit_password_hash('ytUIm8s3AnKsXQjptplKFytfVxI=')
#   print check_rabbit_password('simon','ytUIm8s3AnKsXQjptplKFytfVxI=')


def random_salt():
    """Return random SALT string."""
    return os.urandom(4)


def random_password(length=32):
    """Return a random password."""
    return ''.join(random.choice(string.ascii_letters +
                                 string.digits) for _ in range(length))


def encode_rabbit_password_hash(password, salt=random_salt()):
    """Return BASE64 of PASSWORD HASH as per RabbitMQ specifications."""
    salt_and_password = salt + password.encode('utf-8')
    salted_sha256 = hashlib.sha256(salt_and_password).digest()
    password_hash = base64.b64encode(salt + salted_sha256)
    return password_hash


def decode_rabbit_password_hash(password_hash):
    """Return arrat with SALT and PASSWORD HASH from BASE64."""
    decoded_hash = base64.b64decode(password_hash)
    decoded_hash = decoded_hash.encode('hex')
    return (decoded_hash[0:8], decoded_hash[8:])


def check_rabbit_password(test_password, password_hash):
    """Check if password is valid."""
    salt, hash_sha256sum = decode_rabbit_password_hash(password_hash)
    test_password_hash = encode_rabbit_password_hash(
        test_password, salt.decode('hex'))
    return test_password_hash == password_hash
