#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2018 Stefano Stella <mprenditore@gmail.com>
#
# Distributed under terms of the MIT license.

"""
Welcome to JudgeD, a way to frame RabbitMQ.

(In honor of Judge Doom from "Who framed Roger Rabbit")

Usage:
    judged.py genconfig [options]
    judged.py list (users | vhosts | permissions | missing_perms) [options]
                    [-s <searchString>] [-i | --insensitive]
    judged.py export (users | users_permissions | vhosts | vhosts_data)
                    [options] [-j | --json] [-o <outputFile>]
    judged.py manage (user | permission) [options] [-t <tags>] [-u <username>]
                    [-E | --erase] [-p <userpass>] [-H <userPassHash>]
                    [-v <vhost>] [-c <configPermission>] [-r <readPermission>]
                    [-w <writePermission>]
                    [-F | --forceCreate]
    judged.py clone (user) [options] [--src <source>]
                    [--dst <destination>] [-F | --forceCreate]
    judged.py check (users | vhosts) [options]
                    [-F | --forceCreate] -f <yamlFile>
    judged.py (-h | --help)

Option:
    -h --help                             Show this screen
    -i --insensitive                      Case insesitive search
    -j --json                             Export as JSON, [default: False]
    -E --erase                            Erase [default: False]
    -F, --forceCreate                     Force creation while checking
    -u, --user <username>                 Username to configure
    -p, --userpass <userPass>             Password for user
    -H, --userpasshash <userPassHash>     Password Hash for user
    -v, --vhost <vhost>                   VirtualHost name to configure
    -t, --tags <tags>                     Comma separated tags for user
    -s, --search <searchString>           String to search in the output
    -c, --confPerm <configPermission>     Configuration permission (Regex)
                                          [default: ]
    -r, --readPerm <readPermission>       Read permission (Regex)
                                          [default: .*]
    -w, --writePerm <writePermission>     Write permission (Regex)
                                          [default: ]
    -o, --output <outputFile>             Output file [default: /dev/stdout]
    -f, --yamlFile <yamlFile>             YAML file to check
    --src <source>                        Source to clone
    --dst <destination>                   Destination where to clone to
Options:
    -C, --conf <configFile>               Config file with connection params to
                                          use [default: ~/.judged/default.conf]
    --hostname <hostname>                 Hostname to connect to
    --username <authUser>                 Username to to connect
    --password <authPass>                 Password to to connect
    --no_ssl_verify                       Skip SSL cert verification

TODO

- change user password - cl.create_user(username, password/password_hash, tags)
"""

from docopt import docopt
from pyrabbit2.api import Client
import sys
import os
import yaml
from tabulate import tabulate
import rabbitmq_passwd as rpwd
import time


def check_required(data, args):
    """Check if required parameters are setup."""
    required_map = {'--username': 'Username',
                    '--hostname': 'Hostname',
                    '--password': 'Password',
                    '--conf': 'Configuration File',
                    '--vhost': 'VHost name',
                    '--confPerm': 'Configuraion Permission',
                    '--readPerm': 'Read Permission',
                    '--writePerm': 'Write Permission'}
    for k in args:
        if k in required_map and data.get(k) is None:
            while True:
                data[k] = raw_input(
                    "Insert value of '%s': " % required_map.get(k))
                if len(data[k]) > 0:
                    break
                else:
                    print "Empty Value, retry, you'll be more lucky!"
    return data


def cleanUp(arguments):
    """Clean up arguments."""
    for k, v in arguments.items():
        if v is False or v is None:
            del arguments[k]
    return arguments


def loadYaml(yaml_file):
    """Load and return the YAML file as dict."""
    try:
        with open(os.path.expanduser(yaml_file), 'r') as f:
            cfg = yaml.load(f)
    except IOError:
        print "YAML file \"%s\" not found!" % yaml_file
        return {}
    else:
        return cfg


def filterElement(elements, headers, search=None, insensitive=False,
                  full_search=False):
    """Print elements only if match the search  string."""
    table = []
    search = search.lower() if insensitive else search
    for elem in elements:
        if full_search:
            for e in elem:
                e = str(e).lower() if insensitive else str(e)
                if (search is None or search in e) and elem not in table:
                    table.append(elem)
        else:
            e = str(elem[0]).lower() if insensitive else str(elem[0])
            if (search is None or search in e) and elem not in table:
                table.append(elem)
    print tabulate(table, headers=headers)


def listUsers():
    """Return a dict with all users.

    {
        'headers': [<header1>, <header2>],
        'data': [[<name>, <tag>]]
    }
    """
    users = cl.get_users()
    headers = ['Name', 'Role', 'Hash']
    ret = []
    for u in users:
        if u.get('tags') == '':
            u['tags'] = "NONE"
        ret.append([u.get('name'), u.get('tags'), u.get('password_hash')])
    return {'headers': headers, 'data': ret}


def listVhosts():
    """List Users."""
    vhosts = cl.get_all_vhosts()
    headers = ['Name', 'Messages #']
    ret = []
    for v in vhosts:
        ret.append([v.get('name'), v.get('messages')])
    return {'headers': headers, 'data': ret}


def listPermissions():
    """List user permission on vhosts."""
    permissions = cl.get_permissions()
    headers = ['User', 'VHost', 'Conf', 'Read', 'Write']
    permlist = []
    for p in permissions:
        permlist.append([p.get('user'), p.get('vhost'), p.get('configure'),
                         p.get('read'), p.get('write')])
    return {'headers': headers, 'data': sorted(permlist)}


def listMissingPermissions():
    """List user's missing permissions."""
    vhosts = [vh.get('name') for vh in cl.get_all_vhosts()]
    permissions = cl.get_permissions()
    headers = ['Username', 'Vhost Name']
    missinglist = []
    temp = {}
    perms = {}
    for v in vhosts:
        for p in permissions:
            user = p.get('user')
            vhost = p.get('vhost')
            if user not in perms:
                perms[user] = []
            if vhost not in perms[user]:
                perms[user].append(vhost)
        for user, vhs in perms.items():
            if user not in temp:
                temp[user] = []
            if v not in vhs:
                temp[user].append(v)

    for user, vhs in temp.items():
        missinglist.append([user, ", ".join(vhs)])
    return {'headers': headers, 'data': sorted(missinglist)}


def getUsers():
    """Return Users as custom organized dict."""
    users = cl.get_users()
    out = {}
    for u in users:
        uname = u.get("name")
        out[uname] = {'password_hash': u.get('password_hash'),
                      'tags': u.get('tags')}

    return {"users": out}


def getUsersPermissions():
    """Return extended Users dict with permissions."""
    users = getUsers()['users']
    definitions = cl.get_definitions()
    for p in definitions["permissions"]:
        user = p.get("user")
        vhost = p.get("vhost")
        if "permissions" not in users[user]:
            users[user]["permissions"] = {}
        if vhost not in users[user]["permissions"]:
            users[user]["permissions"][vhost] = {}
        for perm in ["configure", "read", "write"]:
            users[user]["permissions"][vhost][perm] = p.get(perm)
    return users


def getPermissions(users_perms):
    """Return users permissions for a better processing."""
    permissions = {}
    for user, u_data in users_perms.items():
        permissions[user] = {}
        if 'permissions' in u_data:
            for vhost, perm in u_data['permissions'].items():
                permissions[user][vhost] = joinPermissions(
                    perm['configure'], perm['read'], perm['write'])
    return permissions


def getBindings(definitions):
    """Get binding list and extract only the one with custom routing keys."""
    bindings = {}
    for bind in definitions['bindings']:
        rk = bind.get('routing_key')
        source = bind.get('source')
        vhost = bind.get('vhost')
        # if not source.startswith('amq.'):
        if vhost not in bindings:
            bindings[vhost] = {}
        if source not in bindings[vhost]:
            bindings[vhost][source] = {}
        if 'routing_keys' not in bindings[vhost][source]:
            bindings[vhost][source]['routing_keys'] = {}
        if rk not in bindings[vhost][source]['routing_keys']:
            bindings[vhost][source]['routing_keys'][rk] = {'queues': {}}
        que_name = bind.get('destination')
        bindings[vhost][source]['routing_keys'][rk]['queues'][que_name] = {
            'arguments': bind.get('arguments')
        }
    return bindings


def getPolicies(definitions):
    """Get policy list."""
    policies = {}
    for policy in definitions['policies']:
        name = policy.get('name')
        vhost = policy.get('vhost')
        if vhost not in policies:
            policies[vhost] = {}
        if name not in policies[vhost]:
            policies[vhost][name] = {
                'pattern': policy.get("pattern"),
                'name': policy.get("name"),
                'priority': policy.get("priority"),
                'appli-to': policy.get("apply-to"),
                'definition': policy.get("definition"),
            }
    return policies


def getVhosts():
    """Return Vhosts as custom organized dict."""
    vhosts = cl.get_all_vhosts()
    out = {'vhosts': []}
    for v in vhosts:
        if v.get('name') != '/':
            out['vhosts'].append(v.get('name'))
        # out['vhosts'].append(v.get('name'))
    return out


def getVhostShovels(vhost, shovels):
    """Get Shovels by Vhost."""
    shovel = {}
    if shovels == 200:
        return {}
    for s in shovels:
        if vhost == s.get('vhost'):
            s_name = s.get('name')
            s = s.get('value')
            shovel[s_name] = {
                'src-uri': s.get('src-uri'),
                'src-protocol': s.get('src-protocol', 'amqp091'),
                'src-delete-after': s.get('src-delete-after', 'never'),
                'dest-uri': s.get('dest-uri'),
                'dest-protocol': s.get('dest-protocol', 'amqp091'),
                'dest-add-forward-headers': s.get('dest-add-forward-headers'),
                'ack-mode': s.get('ack-mode')
            }
            if 'src-queue' in s:
                shovel[s_name]['src-queue'] = s.get('src-queue')
                shovel[s_name]['dest-queue'] = s.get('dest-queue')
            else:
                shovel[s_name]['src-exchange'] = s.get('src-exchange')
                shovel[s_name]['src-exchange-key'] = s.get('src-exchange-key')
                shovel[s_name]['dest-exchange'] = s.get('dest-exchange')
                shovel[s_name]['dest-exchange-key'] = s.get(
                    'dest-exchange-key')
    return shovel


def getVhostsData():
    """Return Vhosts as custom organized dict."""
    definitions = cl.get_definitions()
    shovels = cl.get_all_shovels()
    bindings = getBindings(definitions)
    policies = getPolicies(definitions)
    # populate is a dict that have 'key' as value to search for in definitions
    # and as 'value' the argument used to subdivide the sub-data
    populate = {'queues': ['durable', 'auto_delete', 'arguments'],
                'exchanges': ['type', 'auto_delete', 'internal']}
    out = {}

    for v_name in getVhosts()['vhosts']:
        if v_name not in out:
            out[v_name] = {}
        if 'shovels' not in out[v_name]:
            out[v_name]['shovels'] = getVhostShovels(v_name, shovels)
        if 'policies' not in out[v_name]:
            out[v_name]['policies'] = {}
        if v_name in policies:
            for name, values in policies[v_name].items():
                out[v_name]['policies'][name] = values
        if v_name in bindings:
            for b in bindings[v_name]:
                if not any(d['name'] == b and d['vhost'] == v_name for d in definitions['exchanges']):  # NOQA
                    if b.startswith('amq.'):
                        e_type = b.split('.')[1]
                    else:
                        e_type = "fanout"
                    definitions['exchanges'].append(
                        {'name': b,
                         'vhost': v_name,
                         'type': e_type,
                         'auto_delete': False,
                         'internal': False})
    for obj_type, fields in populate.items():
        for d in definitions[obj_type]:
            v_name = d.get('vhost')
            if obj_type not in out[v_name]:
                out[v_name][obj_type] = {}
            obj_name = d.get('name')
            if obj_name not in out[v_name][obj_type]:
                out[v_name][obj_type][obj_name] = {}
            for f in fields:
                out[v_name][obj_type][obj_name][f] = d.get(f)
            # if we're cycling through exchanges check if there are custom
            # bindings and in positive case, add them
            if obj_type == 'exchanges' and v_name in bindings and\
                    obj_name in bindings[v_name]:
                out[v_name][obj_type][obj_name]['routing_keys'] = {}
                for exc, exc_data in bindings[v_name][obj_name]['routing_keys'].items():  # NOQA
                    out[v_name][obj_type][obj_name]['routing_keys'][exc] = exc_data  # NOQA
                    if 'bindings' not in out[v_name]:
                        out[v_name]['bindings'] = {}
                    for que_name, que_data in exc_data.get('queues', {}).items():  # NOQA
                        out[v_name]['bindings'][';'.join(
                            [v_name, obj_name, exc, que_name])] = True

    return {"vhosts": out}


def writeYaml(data, out):
    """Write dictionary to file."""
    json_format = True if data.get('--json') else False
    noalias_dumper = yaml.dumper.SafeDumper
    noalias_dumper.ignore_aliases = lambda self, data: True
    with open(data.get('--output'), 'w') as yaml_file:
        yaml.dump(out, yaml_file, default_flow_style=json_format,
                  Dumper=noalias_dumper)
        # yaml.safe_dump(out, yaml_file, default_flow_style=json_format)
    return 'File %s exported correctly' % data.get('--output')


def expandElem(vhost, check, element):
    """Return a combined array from 'defaults' and vhost specific elements."""
    if check['vhosts'][vhost].get('defaults', True) is True and\
            'defaults' in check and element in check['defaults']:
        expanded = dict(check['defaults'][element])
    else:
        expanded = {}
    if element in check['vhosts'][vhost]:
        if type(check['vhosts'][vhost][element]) is str:
            expanded.update({check['vhosts'][vhost][element]: None})
        else:
            expanded.update(check['vhosts'][vhost][element])
    return expanded


def transformName(name, vhost, brand=None):
    """Do transformations on name."""
    name = name.replace('<vhost>', vhost)
    if brand is not None:
        name = name.replace('<brand>', brand)
    return name


def getReplicateToVhosts(vh_data, ele_data):
    """Return a list of all the vhost where to create an element."""
    vhosts = []
    ele_data = {} if ele_data is None else ele_data
    if 'replicate_to_vhosts' in ele_data and \
            type(ele_data["replicate_to_vhosts"]) is list:
        vhosts.extend(ele_data['replicate_to_vhosts'])
    return vhosts


def joinPermissions(config, read, write):
    """Return a string with permission list."""
    return '; '.join(
        ["config: '%s'" % config,
         "read: '%s'" % read,
         "write: '%s'" % write])


def grantUserPermissions(vhost, user, config, read, write, permissions={},
                         create=False):
    """Assign permission to user."""
    if user in permissions:
        current_permission = joinPermissions(config, read, write)
        if vhost in permissions[user]:
            if current_permission != permissions[user][vhost]:
                print "Changing PERMISSIONS for '%s' on '%s' from \"%s\" -> \"%s\" " % (  # NOQA
                    user, vhost, permissions[user][vhost], current_permission)
        else:
            print "Creating PERMISSIONS for '%s' on '%s' - \"conf: %s, read: %s, write: %s\"" % (  # NOQA
                user, vhost, config, read, write)
    else:
        print "Creating PERMISSIONS for '%s' on '%s' - \"conf: %s, read: %s, write: %s\"" % (  # NOQA
            user, vhost, config, read, write)
    if create:
        cl.set_vhost_permissions(vname=vhost, username=user,
                                 config=config, rd=read, wr=write)
    if user not in permissions:
        permissions[user] = {}
    permissions[user][vhost] = joinPermissions(config, read, write)
    return permissions


def checkUsers(data, create=False):
    """Check if users found in the yaml file are present."""
    check = loadYaml(data.get('--yamlFile'))
    users = getUsers()['users']
    users_perms = getUsersPermissions()
    permissions = getPermissions(users_perms)

    for brand in check['brands']:
        for vhost_name, user_perms in check['default']['permissions'].items():
            vhost_name = transformName(vhost_name, brand, brand)
            if create:
                print "######################"
                print "BRAND: %s" % brand
                print "VHOST: %s" % vhost_name
                print "QUE: que.%s.notifications" % brand
                print "----------------------"
            for user, user_data in user_perms.items():
                username = transformName(user, vhost_name, brand)
                tags = user_data.get('tags', '')
                config = transformName(user_data.get('config', ''),
                                       vhost_name, brand)
                read = transformName(user_data.get('read', ''),
                                     vhost_name, brand)
                write = transformName(user_data.get('write', ''),
                                      vhost_name, brand)
                password = user_data.get('password', None)
                password_hash = user_data.get('password_hash', '')
                createUser(username, users, user_data, create, tags, password,
                           password_hash)
                permissions = grantUserPermissions(vhost_name,
                                                   username, config, read,
                                                   write, permissions, create)
    return 0


def checkVhosts(data, create=False):
    """Check if vhosts, exchanges and queues are present."""
    check = loadYaml(data.get('--yamlFile'))
    vh_data = getVhostsData()['vhosts']
    for vhost in check['vhosts']:
        if type(check['vhosts'][vhost]) is str or \
                check['vhosts'][vhost] is None:
            check['vhosts'][vhost] = {}
        if 'brands' in check['vhosts'][vhost]:
            for brand in check['vhosts'][vhost]['brands']:
                for element in ['exchanges', 'queues', 'shovels', 'policies']:
                    vh_data = iamlHandling(
                        element, vhost, vh_data, check, create, brand)
        else:
            for element in ['exchanges', 'queues', 'shovels', 'policies']:
                vh_data = iamlHandling(element, vhost, vh_data, check, create)
    return 0


def iamlHandling(element, vhost, vh_data, check, create, brand=None):
    """Check or apply IAML exchanges to Rabbit."""
    ele_list = expandElem(vhost, check, element)
    for ele_name, ele_data in ele_list.items():
        vh_data = createElement(element, vhost, vh_data, ele_name,
                                ele_data, create, None, brand)
    return vh_data


def createElement(element, target_vhost, vh_data, ele_name, ele_data,
                  create, vhost_org=None, brand=None):
    """Create a element and return the updated current dict."""
    if vhost_org is None:
        vhost_org = target_vhost
    if brand is not None:
        vhost_org = brand
    vhosts = [target_vhost]
    ele_name = transformName(ele_name, vhost_org, brand)

    ele_data = {} if ele_data is None else ele_data
    replicate_vhosts = getReplicateToVhosts(vh_data, ele_data)
    vhosts.extend(replicate_vhosts)
    for vhost in vhosts:
        vhost = transformName(vhost, vhost_org, brand)
        if vhost not in vh_data:
            print "Creating VHOST: '%s'" % vhost
            if create:
                cl.create_vhost(vhost)
                grantUserPermissions(vhost, cl.user, ".*",
                                     ".*", ".*", {}, True)
                # needed to avoid que creation before vhost is up
                time.sleep(0.2)
            vh_data[vhost] = {}

        if element not in vh_data[vhost]:
            vh_data[vhost][element] = {}
        if ele_name not in vh_data[vhost][element] and \
                not ele_name.startswith("amq."):
            print "Creating %s '%s' in '%s'" % (
                element.upper(), ele_name, vhost)
            if create:
                if element == 'queues':
                    que_durable = ele_data.get('durable', True)
                    que_args = ele_data.get('arguments', {
                        'x-message-ttl': 86400000})
                    cl.create_queue(vhost=vhost, name=ele_name,
                                    durable=que_durable, arguments=que_args)
                if element == 'exchanges':
                    exc_type = ele_data.get('type', 'fanout')
                    exc_auto_delete = ele_data.get('auto_delete', False)
                    exc_durable = ele_data.get('durable', False)
                    exc_internal = ele_data.get('internal', False)
                    cl.create_exchange(vhost=vhost, name=ele_name,
                                       xtype=exc_type,
                                       auto_delete=exc_auto_delete,
                                       durable=exc_durable,
                                       internal=exc_internal)
                if element == 'bindings':
                    args = ele_data.get('args')
                    rk_name = ele_data.get('rk_name')
                    que_name = ele_data.get('que_name')
                    exc_name = ele_data.get('exc_name')
                    cl.create_binding(vhost=vhost, exchange=exc_name,
                                      queue=que_name, rt_key=rk_name,
                                      args=args)
                if element == 'shovels':
                    params = ['src-uri', 'src-queue', 'dest-uri', 'dest-queue']
                    for p in params:
                        if p in ele_data:
                            ele_data[p] = ele_data[p].replace('<vhost>', vhost)
                    cl.create_shovel(
                        vhost=vhost, shovel_name=ele_name, **ele_data)
                if element == 'policies':
                    cl.create_policy(
                        vhost=vhost, policy_name=ele_name, **ele_data)
        if element == 'exchanges':
            vh_data = createBindings(
                vhost, vhost_org, vh_data, ele_name, ele_data, create, brand)

    vh_data[vhost][element][ele_name] = {}

    return vh_data


def createBindings(vhost, vhost_org, vh_data, exc_name, exc_data,
                   create=None, brand=None):
    """Create all exchange bindings."""
    if vhost_org is None:
        if brand is None:
            vhost_org = vhost
        else:
            vhost_org = brand
    if exc_data is not None and 'routing_keys' in exc_data:
        for rk_name, rk_data in exc_data['routing_keys'].items():
            if type(rk_data.get('queues'))is str:
                rk_data['queues'] = {rk_data.get('queues'): None}
            for que_name, que_data in rk_data['queues'].items():
                vh_data = createElement('queues', vhost, vh_data,
                                        que_name, que_data, create,
                                        vhost_org, brand)
                rk_data['args'] = que_data.get('arguments', '') if que_data else ''  # NOQA
                rk_data['rk_name'] = transformName(rk_name, vhost_org, brand)
                rk_data['exc_name'] = transformName(exc_name, vhost_org, brand)
                rk_data['que_name'] = transformName(que_name, vhost_org, brand)
                bind = ';'.join([vhost, exc_name, rk_name, que_name])
                vh_data = createElement('bindings', vhost, vh_data, bind,
                                        rk_data, create, vhost_org, brand)
    return vh_data


def managePermissions(data, create):
    """Manage user using parameters passed via CLI."""
    required = ['--user']
    data = check_required(data, required)
    user = data.get('--user')
    vhost = data.get('--vhost')
    config = data.get('--confPerm')
    read = data.get('--readPerm')
    write = data.get('--writePerm')
    manageUser(data, create)
    users_perms = getUsersPermissions()
    permissions = getPermissions(users_perms)
    grantUserPermissions(vhost, user, config, read, write, permissions, create)


def manageUser(data, create):
    """Manage user using parameters passed via CLI."""
    required = ['--user']
    data = check_required(data, required)
    users = getUsers()['users']
    user = data.get('--user')
    password = data.get('--userpass', None)
    password_hash = data.get('--userpasshash', '')
    tags = data.get('--tags', '')
    if data.get('--erase'):
        users = removeUser(user, users, create)
    else:
        users = createUser(user, users, [], create,
                           tags, password, password_hash)
    return users


def removeUser(username, users, create):
    """Remove user."""
    if username in users:
        print "Removing USER '%s'" % username
        del users[username]
        if create:
            cl.delete_user(username)
    return users


def createUser(username, users, user_data, create, tags='', password=None,
               password_hash=''):
    """Create user."""
    if username not in users:
        print "Creating USER '%s'" % username
        if create:
            print "  USER: %s" % username
            if password is None:
                password = rpwd.random_password()
            if password_hash == '':
                password_hash = rpwd.encode_rabbit_password_hash(password)
                print "  PASS: %s" % password
            else:
                print "  PASS: HASHED"
            if "host" in user_data:
                print "  HOST: %s" % user_data['host']
            if "port" in user_data:
                print "  PORT: %s" % user_data['port']
            if "ssl" in user_data:
                print "  SSL: %s" % user_data['ssl']
            print "----------------------"
            cl.create_user(username=username, password_hash=password_hash,
                           tags=tags)
    else:
        if password is not None:
            password_hash = rpwd.encode_rabbit_password_hash(password)
            if password_hash != users[username]['password_hash']:
                print "Changing PASSWORD for USER '%s'" % username
        else:
            if password_hash == '':
                password_hash = users[username]['password_hash']
            else:
                print "Changing PASSWORD via HASH for USER '%s'" % username

        if tags != users[username]['tags']:
            print "Changing TAGs for USER '%s' from '%s' -> '%s'" % (
                username, users[username]['tags'], tags)
        if create:
            cl.create_user(username=username, password_hash=password_hash,
                           tags=tags)
    users.update({username: {'password_hash': password_hash,
                             'tags': tags}})
    return users


def cloneUser(data, create=False):
    """Clone user permissions."""
    required = ['--src', '--dst']
    data = check_required(data, required)
    users = getUsers()['users']
    src = data['--src']
    dst = data['--dst']
    if src in users:
        createUser(dst, users, [], create, users[src]['tags'])
        users_perms = getUsersPermissions()
        permissions = getPermissions(users_perms)
        if 'permissions' in users_perms[src]:
            for vhost, perm in users_perms[src]['permissions'].items():
                config = perm.get('config', '')
                read = perm.get('read', '')
                write = perm.get('write', '')
                permissions = grantUserPermissions(vhost, dst, config, read,
                                                   write, permissions, create)
    else:
        print "User '%s' do not exist" % src


def deleteAllVhosts():
    """Delete all vhosts."""
    for v in getVhosts()['vhosts']:
        print "Deleting '%s'" % v
        cl.delete_vhost(v)


def grantAllPermissions(users=[], vhosts=[]):
    """Grant permission to vhosts to users."""
    for v in vhosts:
        for u in users:
            cl.set_permissions(v, u, '.*', '.*', '.*')
    return 0


def generateConfig(data):
    """Generate configuration file."""
    required = ['--hostname', '--username', '--password']
    conf = loadYaml(data.get('--conf'))
    print "Current configuration:\n%s" % conf
    data = check_required(data, required)

    conf['hostname'] = data.get('--hostname')
    conf['username'] = data.get('--username')
    conf['password'] = data.get('--password')
    with open(data.get('--conf'), 'w') as f:
        f.write(yaml.safe_dump(conf, default_flow_style=False))


def handleList(arguments):
    """List function router."""
    search = arguments.get('--search')
    insensitive = arguments.get('--insensitive')
    full_search = False
    if 'list' in arguments:
        if 'users' in arguments:
            ret = listUsers()
            full_search = True
        if 'vhosts' in arguments:
            ret = listVhosts()
            full_search = True
        if 'permissions' in arguments:
            ret = listPermissions()
        if 'missing_perms' in arguments:
            ret = listMissingPermissions()
    try:
        filterElement(ret['data'], ret['headers'], search, insensitive,
                      full_search)
    except NameError:
        return 1
    else:
        return 0


def handleExport(arguments):
    """Export function router."""
    if 'export' in arguments:
        if 'users' in arguments:
            ret = getUsers()
        if 'users_permissions' in arguments:
            ret = getUsersPermissions()
        if 'vhosts' in arguments:
            ret = getVhosts()
        if 'vhosts_data' in arguments:
            ret = getVhostsData()
    try:
        print writeYaml(arguments, ret)
    except NameError:
        return 1
    else:
        return 0


def handleCheck(arguments):
    """List function router."""
    if 'check' in arguments:
        create = arguments.get('--forceCreate', False)
        if 'users' in arguments:
            ret = checkUsers(arguments, create)
        if 'vhosts' in arguments:
            ret = checkVhosts(arguments, create)
    try:
        ret
    except NameError:
        return 1
    else:
        return 0


def handleManage(arguments):
    """Create function router."""
    if 'manage' in arguments:
        create = arguments.get('--forceCreate', False)
        if 'user' in arguments:
            ret = manageUser(arguments, create)
        if 'permission' in arguments:
            ret = managePermissions(arguments, create)
    try:
        ret
    except NameError:
        return 1
    else:
        return 0


def handleClone(arguments):
    """Clone function router."""
    if 'clone' in arguments:
        create = arguments.get('--forceCreate', False)
        if 'user' in arguments:
            ret = cloneUser(arguments, create)
    try:
        ret
    except NameError:
        return 1
    else:
        return 0


# def handleIAML(arguments):
#     """Create function router."""
#     if 'iaml' in arguments:
#         checkVhosts(arguments, True)
#     return 0


def handleGenerate(arguments):
    """Generate configuration file."""
    if 'genconfig' in arguments:
        sys.exit(generateConfig(arguments))


if __name__ == '__main__':
    arguments = cleanUp(docopt(__doc__, version='1.0'))
    if arguments.get('--no_ssl_verify') is None:
        verify = True
    else:
        verify = False
    cfg = loadYaml(arguments.get('--conf'))
    if not cfg:
        required = ['--hostname', '--username', '--password']
        arguments = check_required(arguments, required)
        cfg['hostname'] = arguments.get('--hostname')
        cfg['username'] = arguments.get('--username')
        cfg['password'] = arguments.get('--password')
        # print cfg
        # sys.exit(1)
    endpoint = cfg['hostname'].replace('://', ';').split(';')
    if len(endpoint) > 1:
        cfg['scheme'] = endpoint[0]
        cfg['hostname'] = endpoint[1]
    else:
        cfg['scheme'] = 'https'
        cfg['hostname'] = endpoint[0]
    cl = Client(cfg['hostname'], cfg['username'],
                cfg['password'], scheme=cfg['scheme'], timeout=15,
                verify=verify)

    handleManage(arguments)
    handleList(arguments)
    handleExport(arguments)
    handleCheck(arguments)
    handleClone(arguments)
    handleGenerate(arguments)
